// querySelector() method returns the first element that matches a selector

const txtFirstName = document.querySelector("#txt-first-name");
const spanFullName = document.querySelector("#span-full-name");

// addEventListener() - method attaches an event handler to an element
// keyup - A keyboard key is released after being pushed

txtFirstName.addEventListener("keyup", (event) => {
    spanFullName.innerHTML = txtFirstName.value;
});

/*
    ? What is event target value
    * event.target - gives you the element that triggered the event
    * event.target.value - return the element where the event occure
*/
txtFirstName.addEventListener("keyup", (event) => {
    console.log(event.target);
    console.log(event.target.value);
});